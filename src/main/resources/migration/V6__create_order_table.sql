create table orders (
    id int primary key auto_increment,
    commodity_id int not null,
    count int not null,
    foreign key (commodity_id) references commodities(id)
) charset=utf8;