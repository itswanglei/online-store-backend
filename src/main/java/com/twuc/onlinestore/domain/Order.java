package com.twuc.onlinestore.domain;

import javax.persistence.*;

@Entity(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "commodity_id")
    private Commodity commodity;
    private int count;

    public Order() {
    }

    public Order(Commodity commodity, Integer count) {
        this.commodity = commodity;
        this.count = count;
    }

    public Integer getId() {
        return id;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public int getCount() {
        return count;
    }

    public void addCount() {
        this.count++;
    }
}
