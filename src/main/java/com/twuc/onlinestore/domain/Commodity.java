package com.twuc.onlinestore.domain;

import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity(name = "commodities")
public class Commodity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotNull
    @Length(min = 1,  max = 50)
    @Column(nullable = false, length = 50)
    private String name;
    @NotNull
    @Length(min = 1, max = 10)
    @Column(nullable = false, length = 10)
    private String price;
    @NotNull
    @Length(min = 1, max = 10)
    @Column(nullable = false, length = 10)
    private String unit;
    @NotNull
    @Length(min = 1, max = 100)
    @Column(name = "image_url", nullable = false, length = 100)
    private String imageUrl;

    public Commodity() {
    }

    public Commodity(String name, String price, String unit, String imageUrl) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.imageUrl = imageUrl;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
