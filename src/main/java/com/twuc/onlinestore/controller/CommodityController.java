package com.twuc.onlinestore.controller;

import com.twuc.onlinestore.domain.Commodity;
import com.twuc.onlinestore.service.CommodityService;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/commodities")
@CrossOrigin(origins = "*")
public class CommodityController {

    private CommodityService service;

    public CommodityController(CommodityService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity create(@RequestBody @Valid Commodity commodity) {
        Commodity createdCommodity = service.create(commodity);
        return ResponseEntity
                .created(linkTo(methodOn(CommodityController.class).getById(createdCommodity.getId())).toUri())
                .build();
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable Integer id) {
        return null;
    }

    @GetMapping
    public ResponseEntity getAll() {
        List<Resource> resources = service.getAll()
                .stream()
                .map(commodity -> new Resource(commodity, linkTo(methodOn(CommodityController.class).getById(commodity.getId())).withSelfRel()))
                .collect(Collectors.toList());
        return ResponseEntity.ok(resources);
    }
}
