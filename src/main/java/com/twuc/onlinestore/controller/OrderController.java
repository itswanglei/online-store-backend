package com.twuc.onlinestore.controller;

import com.twuc.onlinestore.contract.CreateOrderRequest;
import com.twuc.onlinestore.contract.GetOrderResponse;
import com.twuc.onlinestore.domain.Order;
import com.twuc.onlinestore.service.OrderService;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/orders")
@CrossOrigin(origins = "*")
public class OrderController {

    private OrderService service;

    public OrderController(OrderService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity create(@RequestBody @Valid CreateOrderRequest request) {
        Order createdOrder = service.create(request);
        return ResponseEntity
                .created(linkTo(methodOn(OrderController.class).getById(createdOrder.getId())).toUri())
                .build();
    }

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable Integer id) {
        return null;
    }

    @GetMapping
    public ResponseEntity getAll() {
        List<GetOrderResponse> responses = service.getAll()
                .stream()
                .map(order -> new GetOrderResponse(order))
                .collect(Collectors.toList());
        return ResponseEntity.ok(responses);
    }

    @DeleteMapping("/{commodityName}")
    public ResponseEntity deleteByCommodityName(@PathVariable String commodityName) {
        service.deleteByCommodityName(commodityName);
        return ResponseEntity.ok().build();
    }
}
