package com.twuc.onlinestore.service;

import com.twuc.onlinestore.contract.ExistedCommodityException;
import com.twuc.onlinestore.dao.CommodityRepository;
import com.twuc.onlinestore.domain.Commodity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CommodityService {

    private CommodityRepository repository;

    public CommodityService(CommodityRepository repository) {
        this.repository = repository;
    }

    public Commodity create(Commodity commodity) {
        if (this.getByName(commodity.getName()) != null) {
            throw new ExistedCommodityException();
        }
        return repository.save(commodity);
    }

    public List<Commodity> getAll() {
        return repository.findAll();
    }

    public Commodity getByName(String name) {
        return repository.findByName(name);
    }
}
