package com.twuc.onlinestore.dao;

import com.twuc.onlinestore.domain.Commodity;
import com.twuc.onlinestore.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    Optional<Order> findByCommodity(Commodity commodity);
}
