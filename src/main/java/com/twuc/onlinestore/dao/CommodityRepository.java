package com.twuc.onlinestore.dao;

import com.twuc.onlinestore.domain.Commodity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommodityRepository extends JpaRepository<Commodity, Integer> {
    Commodity findByName(String name);
}
