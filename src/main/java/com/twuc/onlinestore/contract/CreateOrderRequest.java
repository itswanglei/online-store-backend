package com.twuc.onlinestore.contract;

import javax.validation.constraints.NotNull;

public class CreateOrderRequest {

    @NotNull
    private String commodityName;

    public CreateOrderRequest(@NotNull String commodityName) {
        this.commodityName = commodityName;
    }

    public CreateOrderRequest() {
    }

    public String getCommodityName() {
        return commodityName;
    }
}
