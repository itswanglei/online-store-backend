package com.twuc.onlinestore.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommodityTest {

    @Test
    void should_create_commodity_object() {
        Commodity cola = new Commodity("cola", "5.5", "b", "www.cola.com");
        assertEquals("cola", cola.getName());
        assertEquals("5.5", cola.getPrice());
        assertEquals("b", cola.getUnit());
        assertEquals("www.cola.com", cola.getImageUrl());
    }
}
