package com.twuc.onlinestore.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderTest {

    @Test
    void should_create_order() {
        Commodity commodity = new Commodity("cola", "5", "box", "www.image.com");
        Order order = new Order(commodity, 2);

        assertEquals(commodity, order.getCommodity());
        assertEquals(2, order.getCount());
    }
}
