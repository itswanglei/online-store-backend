package com.twuc.onlinestore.dao;

import com.twuc.onlinestore.IntegrationTestBase;
import com.twuc.onlinestore.domain.Commodity;
import com.twuc.onlinestore.domain.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderRepositoryTest extends IntegrationTestBase {

    @Autowired
    OrderRepository repository;

    @Test
    void should_save_order() {
        Commodity commodity = new Commodity("cola", "5", "box", "www.image.com");
        Order order = new Order(commodity, 2);
        Order savedOrder = repository.save(order);
        assertEquals(order, savedOrder);
    }
}
