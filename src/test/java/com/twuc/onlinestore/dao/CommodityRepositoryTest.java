package com.twuc.onlinestore.dao;

import com.twuc.onlinestore.IntegrationTestBase;
import com.twuc.onlinestore.domain.Commodity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CommodityRepositoryTest extends IntegrationTestBase {

    @Autowired
    CommodityRepository repository;

    @Test
    void should_create_commodity() {
        Commodity cola = new Commodity("cola", "5.5", "b", "www.cola.com");
        Commodity savedCola = repository.save(cola);
        assertEquals(cola, savedCola);
    }

    @Test
    void should_find_commodity_by_name() {
        Commodity cola = new Commodity("cola", "5.5", "b", "www.cola.com");
        Commodity savedCola = repository.save(cola);

        Commodity foundedCola = repository.findByName("cola");

        assertEquals(savedCola, foundedCola);
    }

    @Test
    void should_find_all_commodities() {
        Commodity cola = new Commodity("cola", "5.5", "b", "www.cola.com");
        Commodity sprite = new Commodity("sprite", "5.5", "b", "www.cola.com");
        repository.save(cola);
        repository.save(sprite);
        getEm().flush();
        getEm().clear();

        List<Commodity> commodities = repository.findAll();
        assertEquals(2, commodities.size());
    }
}
